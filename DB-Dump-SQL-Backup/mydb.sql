-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 02. Jun 2017 um 17:42
-- Server-Version: 10.1.21-MariaDB
-- PHP-Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `mydb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `autoangebote`
--

CREATE TABLE `autoangebote` (
  `aan_id` int(10) UNSIGNED NOT NULL,
  `am_f_id` int(10) UNSIGNED NOT NULL,
  `Kilometerstand` int(11) DEFAULT NULL,
  `Erstzulassung` date DEFAULT NULL,
  `Bildpfad` varchar(100) DEFAULT NULL,
  `Preis` decimal(10,2) UNSIGNED NOT NULL COMMENT 'Preis in Euro. 2 stellen hinterm komma',
  `HU` date NOT NULL COMMENT 'Datum des naechstens TUEVS/HU',
  `AnzTueren` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `autoangebote`
--

INSERT INTO `autoangebote` (`aan_id`, `am_f_id`, `Kilometerstand`, `Erstzulassung`, `Bildpfad`, `Preis`, `HU`, `AnzTueren`) VALUES
(4, 1, 122222, '2008-08-01', 'opel-astra-cd.jpg', '1000.00', '0000-00-00', 0),
(5, 2, 120000, '2000-04-01', 'opel-corsa-Edition2000cool.jpg', '700.00', '0000-00-00', 0),
(6, 5, 30000, '2008-08-01', 'Ford-Mustang-2015-blau.jpg', '30000.00', '0000-00-00', 0),
(7, 8, 17000, '2014-10-01', 'bmw-m4.jpg', '65000.00', '2020-10-01', 0),
(8, 1, 2147483647, '2008-08-01', 'opel-astra-cd.jpg', '500.00', '0000-00-00', 0),
(10, 10, 165000, '1992-04-01', 'nissan-200sx-nr3-rot.jpg', '6500.00', '2017-08-01', 0),
(11, 11, 243000, '1989-09-01', 'nissan-200sx-nr4-stage2-241ps.jpg', '6000.00', '2018-08-01', 0),
(12, 10, 279564, '1991-12-01', 'nissan-200sx-nr2-gruen.jpg', '4100.00', '2017-03-01', 0),
(13, 10, 130000, '1992-12-01', 'nissan-200sx-nr1.jpg', '4500.00', '2017-06-01', 0),
(14, 3, 434454, '2016-06-06', 'opel-astra-cd.jpg', '444.00', '2000-06-06', 0),
(15, 3, 434454, '2016-06-06', 'opel-astra-cd.jpg', '444.00', '2000-06-06', 0),
(16, 11, 434454, '2000-06-06', 'nissan-200sx-nr3-rot.jpg', '6500.00', '2016-06-06', 3),
(17, 11, 434454, '2000-06-06', 'nissan-200sx-nr3-rot.jpg', '6500.00', '2016-06-06', 3),
(18, 5, 121342, '2007-06-06', 'nissan-Ford-Mustang-2015-blauNr2.jpg', '6500.00', '2016-06-06', 3),
(19, 5, 121342, '2007-06-06', 'Ford-Mustang-2015-blauNr2.jpg', '6500.00', '2016-06-06', 3),
(20, 1, 0, '0000-00-00', '.jpg', '0.00', '0000-00-00', 0),
(21, 1, 0, '0000-00-00', '.jpg', '0.00', '0000-00-00', 0),
(22, 1, 165000, '0000-00-00', 'opel-corsa-Edition2000cool.jpg', '3000.00', '2018-03-20', 3),
(23, 1, 22342343, '0000-00-00', '.jpg', '2333.00', '0000-00-00', 3),
(24, 1, 333333, '0000-00-00', '.jpg', '33331.00', '2018-04-01', 3),
(25, 1, 333333, '2017-01-03', '.jpg', '33331.00', '2018-04-01', 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `automodelle`
--

CREATE TABLE `automodelle` (
  `am_id` int(11) UNSIGNED NOT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `Kategorie` varchar(30) DEFAULT NULL,
  `Hubraum` int(11) DEFAULT NULL,
  `Leistung` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `automodelle`
--

INSERT INTO `automodelle` (`am_id`, `Name`, `Kategorie`, `Hubraum`, `Leistung`) VALUES
(1, 'Opel Astra CD ', 'Limousine', 1598, 74),
(2, 'Opel Corsa Edition 2000 Cool ', 'Kleinwagen', 973, 40),
(3, 'Volkswagen Golf 1.4 CL ', 'Limousine', 1391, 44),
(5, 'Ford Mustang 2.3 Eco Boost', 'Sportwagen / Coupe', 2300, 233),
(8, 'BMW 4', 'Sportwagen ', 2979, 317),
(10, 'Nissan 200 Turbo 16V SX', 'Sportwagen / Coupee ', 1809, 124),
(11, 'Nissan 200SX Stage 2 241 PS', 'Sportwagen/Coupee ', 1809, 177);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `meine_spiele`
--

CREATE TABLE `meine_spiele` (
  `Id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Genre` varchar(100) NOT NULL,
  `Wertung` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `meine_spiele`
--

INSERT INTO `meine_spiele` (`Id`, `Name`, `Genre`, `Wertung`) VALUES
(1, 'Warframe', 'Shooter-RPG_MMO', 10),
(2, 'Destiny', 'Shooter-RPG-MMO', 10),
(3, 'Division, The, Tom Clancy\'s', 'Shooter-RPG-MMO', 9);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stellenangebote`
--

CREATE TABLE `stellenangebote` (
  `st_id` int(10) UNSIGNED NOT NULL,
  `unt_id` int(10) UNSIGNED NOT NULL,
  `Stellenbeschreibung` varchar(1000) DEFAULT NULL,
  `Eintrittsdatum` date DEFAULT NULL,
  `Erwartungen` varchar(1000) DEFAULT NULL,
  `Geboten` varchar(100) DEFAULT NULL,
  `Gehalt` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `stellenangebote`
--

INSERT INTO `stellenangebote` (`st_id`, `unt_id`, `Stellenbeschreibung`, `Eintrittsdatum`, `Erwartungen`, `Geboten`, `Gehalt`) VALUES
(1, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Prod', NULL, NULL, NULL, NULL),
(2, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Prod', NULL, NULL, NULL, NULL),
(3, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Prod', NULL, NULL, NULL, NULL),
(4, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Prod', NULL, NULL, NULL, NULL),
(5, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Prod', NULL, NULL, NULL, NULL),
(6, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Prod', NULL, NULL, NULL, NULL),
(7, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Prod', NULL, NULL, NULL, NULL),
(8, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Prod', NULL, NULL, NULL, NULL),
(9, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Prod', NULL, NULL, NULL, NULL),
(10, 0, 'Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Produkte. </br>Kenntnisse im vorbeugenden Brandschutz von Vorteil. </br>Fahrerlaubnis der Klasse B . </br>Eigenverantwortliche und konzeptionelle Arbeitsweise. </br>Serviceorientierung. </br>AusgeprÃ¤gte KommunikationsstÃ¤rke und DurchsetzungsvermÃ¶gen. </br>Hohes MaÃŸ an Teamgeist und Verhandlungsgeschick. </br>Als ServicehÃ¤ndler versorgt Schloemer mehr als 5.000 Kunden mit 100.000 starken Markenprodukten und individuellen Dienstleistungen aus den Bereichen Arbeitsschutz, Schlauchtechnik, Brandschutz, FÃ¶rderbÃ¤nder und Betriebseinrichtungen.. </br>Das Zusammenspiel aus 160 qualifizierten Mitarbeitern, einem hochmodernen Logistikzentrum und elektronischen BeschaffungslÃ¶sungen sorgt fÃ¼r beschleunigte und zugleich sichere Prozesse beim Kunden.. </br>Unsere Kunden unter anderem aus den Branchen Chemie, Energie, Automobile und Dienstleistungen senken durch den Schloemer-Service Arbeits- und ', NULL, NULL, NULL, NULL),
(11, 0, 'Einkauf und Verkauf von Brandschutzeinrichtungen und technischen Schildern.. </br>Koordination von Wartungen im Brandschutzbereich.. </br>Preisverhandlungen mit Kunden und Lieferanten.. </br>Technische Produktberatung und Brandschutzbegehung.. </br>Verkaufsberatung und Betreuung von Bestands- und Neukunden vor Ort beim Kunden.. </br>Bestellwesen und Auftragsabwicklung im ERP-System Dynamics NAV.. </br>Angebotserstellung und -Nachverfolgung.. </br>SelbstÃ¤ndige Abwicklung von ProjektauftrÃ¤gen.. </br>Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Produkte. </br>Kenntnisse im vorbeugenden Brandschutz von Vorteil. </br>Fahrerlaubnis der Klasse B . </br>Eigenverantwortliche und konzeptionelle Arbeitsweise. </br>Serviceorientierung. </br>AusgeprÃ¤gte KommunikationsstÃ¤rke und DurchsetzungsvermÃ¶gen. </br>Hohes MaÃŸ an Teamgeist und Verhandlungsgeschick. </br>Als ServicehÃ¤ndler versorgt Schloemer mehr als 5.000 Kunden mit 100.000 starken Mark', NULL, NULL, NULL, NULL),
(12, 0, 'Einkauf und Verkauf von Brandschutzeinrichtungen und technischen Schildern.. </br>Koordination von Wartungen im Brandschutzbereich.. </br>Preisverhandlungen mit Kunden und Lieferanten.. </br>Technische Produktberatung und Brandschutzbegehung.. </br>Verkaufsberatung und Betreuung von Bestands- und Neukunden vor Ort beim Kunden.. </br>Bestellwesen und Auftragsabwicklung im ERP-System Dynamics NAV.. </br>Angebotserstellung und -Nachverfolgung.. </br>SelbstÃ¤ndige Abwicklung von ProjektauftrÃ¤gen.. </br>Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Produkte. </br>Kenntnisse im vorbeugenden Brandschutz von Vorteil. </br>Fahrerlaubnis der Klasse B . </br>Eigenverantwortliche und konzeptionelle Arbeitsweise. </br>Serviceorientierung. </br>AusgeprÃ¤gte KommunikationsstÃ¤rke und DurchsetzungsvermÃ¶gen. </br>Hohes MaÃŸ an Teamgeist und Verhandlungsgeschick. </br>Als ServicehÃ¤ndler versorgt Schloemer mehr als 5.000 Kunden mit 100.000 starken Mark', NULL, NULL, NULL, NULL),
(13, 0, '327Einkauf und Verkauf von Brandschutzeinrichtungen und technischen Schildern.. </br>327Koordination von Wartungen im Brandschutzbereich.. </br>327Preisverhandlungen mit Kunden und Lieferanten.. </br>327Technische Produktberatung und Brandschutzbegehung.. </br>327Verkaufsberatung und Betreuung von Bestands- und Neukunden vor Ort beim Kunden.. </br>327Bestellwesen und Auftragsabwicklung im ERP-System Dynamics NAV.. </br>327Angebotserstellung und -Nachverfolgung.. </br>327SelbstÃ¤ndige Abwicklung von ProjektauftrÃ¤gen.. </br>327Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>327Berufserfahrung im Verkauf technischer Produkte. </br>327Kenntnisse im vorbeugenden Brandschutz von Vorteil. </br>327Fahrerlaubnis der Klasse B . </br>327Eigenverantwortliche und konzeptionelle Arbeitsweise. </br>327Serviceorientierung. </br>327AusgeprÃ¤gte KommunikationsstÃ¤rke und DurchsetzungsvermÃ¶gen. </br>327Hohes MaÃŸ an Teamgeist und Verhandlungsgeschick. </br>327Als ServicehÃ¤ndler versorgt Schlo', NULL, NULL, NULL, NULL),
(14, 0, 'Ihr Aufgabengebiet Einkauf und Verkauf von Brandschutzeinrichtungen und technischen Schildern.. </br>Ihr Aufgabengebiet Koordination von Wartungen im Brandschutzbereich.. </br>Ihr Aufgabengebiet Preisverhandlungen mit Kunden und Lieferanten.. </br>Ihr Aufgabengebiet Technische Produktberatung und Brandschutzbegehung.. </br>Ihr Aufgabengebiet Verkaufsberatung und Betreuung von Bestands- und Neukunden vor Ort beim Kunden.. </br>Ihr Aufgabengebiet Bestellwesen und Auftragsabwicklung im ERP-System Dynamics NAV.. </br>Ihr Aufgabengebiet Angebotserstellung und -Nachverfolgung.. </br>Ihr Aufgabengebiet SelbstÃ¤ndige Abwicklung von ProjektauftrÃ¤gen.. </br>Ihr Aufgabengebiet Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Ihr Aufgabengebiet Berufserfahrung im Verkauf technischer Produkte. </br>Ihr Aufgabengebiet Kenntnisse im vorbeugenden Brandschutz von Vorteil. </br>Ihr Aufgabengebiet Fahrerlaubnis der Klasse B . </br>Ihr Aufgabengebiet Eigenverantwortliche und konzeptionelle Arbeit', NULL, NULL, NULL, NULL),
(15, 0, 'Ihr Aufgabengebiet Einkauf und Verkauf von Brandschutzeinrichtungen und technischen Schildern.. </br>Koordination von Wartungen im Brandschutzbereich.. </br>Preisverhandlungen mit Kunden und Lieferanten.. </br>Technische Produktberatung und Brandschutzbegehung.. </br>Verkaufsberatung und Betreuung von Bestands- und Neukunden vor Ort beim Kunden.. </br>Bestellwesen und Auftragsabwicklung im ERP-System Dynamics NAV.. </br>Angebotserstellung und -Nachverfolgung.. </br>SelbstÃ¤ndige Abwicklung von ProjektauftrÃ¤gen.. </br>Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Produkte. </br>Kenntnisse im vorbeugenden Brandschutz von Vorteil. </br>Fahrerlaubnis der Klasse B . </br>Eigenverantwortliche und konzeptionelle Arbeitsweise. </br>Serviceorientierung. </br>AusgeprÃ¤gte KommunikationsstÃ¤rke und DurchsetzungsvermÃ¶gen. </br>Hohes MaÃŸ an Teamgeist und Verhandlungsgeschick. </br>Als ServicehÃ¤ndler versorgt Schloemer mehr als 5.000 Kunden mit 1', NULL, NULL, NULL, NULL),
(16, 0, 'Ihr Aufgabengebiet Einkauf und Verkauf von Brandschutzeinrichtungen und technischen Schildern.. </br>Koordination von Wartungen im Brandschutzbereich.. </br>Preisverhandlungen mit Kunden und Lieferanten.. </br>Technische Produktberatung und Brandschutzbegehung.. </br>Verkaufsberatung und Betreuung von Bestands- und Neukunden vor Ort beim Kunden.. </br>Bestellwesen und Auftragsabwicklung im ERP-System Dynamics NAV.. </br>Angebotserstellung und -Nachverfolgung.. </br>SelbstÃ¤ndige Abwicklung von ProjektauftrÃ¤gen.. </br>Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Produkte. </br>Kenntnisse im vorbeugenden Brandschutz von Vorteil. </br>Fahrerlaubnis der Klasse B . </br>Eigenverantwortliche und konzeptionelle Arbeitsweise. </br>Serviceorientierung. </br>AusgeprÃ¤gte KommunikationsstÃ¤rke und DurchsetzungsvermÃ¶gen. </br>Hohes MaÃŸ an Teamgeist und Verhandlungsgeschick. </br>Als ServicehÃ¤ndler versorgt Schloemer mehr als 5.000 Kunden mit 1', NULL, NULL, NULL, NULL),
(17, 0, 'Ihr Aufgabengebiet : Einkauf und Verkauf von Brandschutzeinrichtungen und technischen Schildern.. </br>Koordination von Wartungen im Brandschutzbereich.. </br>Preisverhandlungen mit Kunden und Lieferanten.. </br>Technische Produktberatung und Brandschutzbegehung.. </br>Verkaufsberatung und Betreuung von Bestands- und Neukunden vor Ort beim Kunden.. </br>Bestellwesen und Auftragsabwicklung im ERP-System Dynamics NAV.. </br>Angebotserstellung und -Nachverfolgung.. </br>SelbstÃ¤ndige Abwicklung von ProjektauftrÃ¤gen.. </br>Abgeschlossene technisch-kaufmÃ¤nnische Ausbildung. </br>Berufserfahrung im Verkauf technischer Produkte. </br>Kenntnisse im vorbeugenden Brandschutz von Vorteil. </br>Fahrerlaubnis der Klasse B . </br>Eigenverantwortliche und konzeptionelle Arbeitsweise. </br>Serviceorientierung. </br>AusgeprÃ¤gte KommunikationsstÃ¤rke und DurchsetzungsvermÃ¶gen. </br>Hohes MaÃŸ an Teamgeist und Verhandlungsgeschick. </br>Als ServicehÃ¤ndler versorgt Schloemer mehr als 5.000 Kunden mit', NULL, NULL, NULL, NULL),
(18, 0, 'Ihr Aufgabengebiet : Anfertigung und PrÃ¼fung von Industrieschlauchleitungen mit unterschiedlichen Einbindesystemen. </br>Externe Wartung von Schlauchleitungen. </br>Interne und externe Montage von Schlauchaufrollsystemen und Schlauchgelenkarmen. </br>Erneuerung von Schlauchleitungen aller Art im Rahmen von AuÃƒÂŸendiensteinsÃƒÂ¤tzen. </br>Koordination und Organisation der AuÃƒÂŸendiensttermine. </br>Abgeschlossene handwerkliche Ausbildung. </br>FÃ¼hrerschein. </br>Kundenorientierung. </br>Eine selbstÃ¤ndige, zuverlÃ¤ssige und prÃ¤zise Arbeitsweise. </br>KÃ¶rperliche Belastbarkeit. </br>FlexibilitÃ¤t . </br>Sicheres und kommunikatives Auftreten. </br>Talent fÃƒÂ¼r VertriebsgesprÃƒÂ¤che. </br>Als ServicehÃ¤ndler versorgt Schloemer mehr als 5.000 Kunden mit 100.000 starken Markenprodukten und individuellen Dienstleistungen aus den Bereichen Arbeitsschutz, Schlauchtechnik, Brandschutz, FÃ¶rderbÃ¤nder und Betriebseinrichtungen.. </br>Das Zusammenspiel aus 160 qualifizierten Mitarbeitern, e', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tgb`
--

CREATE TABLE `tgb` (
  `id` int(11) NOT NULL,
  `Eintrag` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tgb`
--

INSERT INTO `tgb` (`id`, `Eintrag`) VALUES
(1, ' Ah ok. Habe auch Schwierigkeiten damit. Drücke dir Daumen. Teils erschöpft und zu spät ins bett ubd etwas einsam.                          Heute aber produktiv, Termine, far cry 3 auf schwer, wald spazieren, vorbereitung auf Vorstellungsgespräch und programmieren zur Vorbereitung auf probearbeiten ab nächste Woche 4.10. &#128522;&#9996;&#127995;'),
(2, '[17:03, 28.9.2016] Tobias Diekmann: Bambi hatte nie eine Chance... &#128521; Wollte gerade meine Oma besuchen, die hat sich das Bein gebrochen und liegt in Münster im Krankenhaus. Habe aber eben von Tante erfahren, dass Oma heute schon Besuch hat, dann fahr ich lieber erst die nächsten Tage, evtl Sonntag, das wird meiner Oma sonst zu viel.                         \r\n[17:45, 28.9.2016] +49 1577 0471854: Ohhh gute Besserung an omi                         \r\n[17:46, 28.9.2016] +49 1577 0471854: Hatte super wald dartblaster aussichtswiesen Spaziergang &#128154;&#127796;&#127795;                                                                           \r\n[18:54, 28.9.2016] Tobias Diekmann: Danke! Das untere Bild sieht wie in einem erstklassigen NextGen-Shooter aus. Wir brauchen einen Titel! &#128521; Deine Wiese ist hübsch.                         \r\n[18:56, 28.9.2016] Tobias Diekmann: Call of Duty - Green Ops?                         \r\n[19:06, 28.9.2016] +49 1577 0471854: Danke. Cod Green ops');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `unternehmen`
--

CREATE TABLE `unternehmen` (
  `unt_id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `Webseite` varchar(100) DEFAULT NULL,
  `Unternehmensbeschreibung` varchar(100) DEFAULT NULL,
  `Strasse` varchar(100) DEFAULT NULL,
  `StrNr` int(10) UNSIGNED DEFAULT NULL,
  `Ort` varchar(30) DEFAULT NULL,
  `PLZ` decimal(5,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `unternehmen`
--

INSERT INTO `unternehmen` (`unt_id`, `Name`, `Webseite`, `Unternehmensbeschreibung`, `Strasse`, `StrNr`, `Ort`, `PLZ`) VALUES
(0, 'http://www.schloemer24.de/', 'http://www.schloemer24.de/', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passwort` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vorname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nachname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `user`, `passwort`, `vorname`, `nachname`, `created_at`, `updated_at`) VALUES
(1, 'test@test.de', '$2y$10$rC7dPIDHqhQl60QcpFk9aeGQiEiB0iIfhwu/xyCNdCYaxI3CzIw7m', '', '', '2016-10-24 19:21:09', NULL),
(2, 'CorvusRohan@yahoo.de', '$2y$10$/YFowVCTBsVTnKLU56ceaO1HtopeVfkfGdMALwUbTTovMck2FnswK', '', '', '2016-10-24 19:29:25', NULL);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `autoangebote`
--
ALTER TABLE `autoangebote`
  ADD PRIMARY KEY (`aan_id`),
  ADD KEY `am_f_id` (`am_f_id`);

--
-- Indizes für die Tabelle `automodelle`
--
ALTER TABLE `automodelle`
  ADD PRIMARY KEY (`am_id`);

--
-- Indizes für die Tabelle `meine_spiele`
--
ALTER TABLE `meine_spiele`
  ADD PRIMARY KEY (`Id`);

--
-- Indizes für die Tabelle `stellenangebote`
--
ALTER TABLE `stellenangebote`
  ADD PRIMARY KEY (`st_id`),
  ADD KEY `unt_id` (`unt_id`);

--
-- Indizes für die Tabelle `tgb`
--
ALTER TABLE `tgb`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `unternehmen`
--
ALTER TABLE `unternehmen`
  ADD PRIMARY KEY (`unt_id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`user`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `autoangebote`
--
ALTER TABLE `autoangebote`
  MODIFY `aan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT für Tabelle `automodelle`
--
ALTER TABLE `automodelle`
  MODIFY `am_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `meine_spiele`
--
ALTER TABLE `meine_spiele`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `stellenangebote`
--
ALTER TABLE `stellenangebote`
  MODIFY `st_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT für Tabelle `tgb`
--
ALTER TABLE `tgb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `autoangebote`
--
ALTER TABLE `autoangebote`
  ADD CONSTRAINT `autoangebote_ibfk_1` FOREIGN KEY (`am_f_id`) REFERENCES `automodelle` (`am_id`) ON UPDATE CASCADE;

--
-- Constraints der Tabelle `stellenangebote`
--
ALTER TABLE `stellenangebote`
  ADD CONSTRAINT `stellenangebote_ibfk_1` FOREIGN KEY (`unt_id`) REFERENCES `unternehmen` (`unt_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
