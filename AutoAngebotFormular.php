<html lang="de">
<head>
<title>Neues Auto Modell einfügen</title>

<!--Bootstrap Date-Picker via https://formden.com/form-builder/ -->
		<!-- Special version of Bootstrap that only affects content wrapped in .bootstrap-iso -->
		<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

		<!--Font Awesome (added because you use icons in your prepend/append)-->
		<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />

		<!-- Inline CSS based on choices in "Settings" tab -->
		<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>

	
		<!-- Extra JavaScript/CSS added manually in "Settings" tab -->
		<!-- Include jQuery -->
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

		<!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

		<script>
			$(document).ready(function(){
				var date_input=$('input[name="Erstzulassung"]'); //our date input has the name "Erstzulassung"
				var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
				date_input.datepicker({
					//format: 'mm/dd/yyyy' vorher soll aber deutsch sein:
					format: 'dd.mm.yyyy',
					container: container,
					todayHighlight: true,
					autoclose: true,
				})
			})
			$(document).ready(function(){
				var date_input=$('input[name="HU"]'); //our date input has the name "Erstzulassung"
				var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
				date_input.datepicker({
					//format: 'mm/dd/yyyy' vorher soll aber deutsch sein:
					format: 'dd.mm.yyyy',
					container: container,
					todayHighlight: true,
					autoclose: true,
				})
			})
		</script>
		
<!--/End-Bootstrap-Date-Picker-->

</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<body>
	<form action="AutoAngebotEintragen.php" method="post">
		Automodell auswaehlen (wurde von einem MA eingetragen): 
		</br>

		<select name="Automodelle-select">
			<?php 
					include_once('classes/model/connectionMy.php');
					$pdo = ConnectionMy::connect();
					$strSQL = "SELECT * FROM automodelle";
					//short syntax http://php.net/manual/de/pdo.query.php
                    foreach ($pdo->query($strSQL) as $row) {
						echo "<option value=".$row['am_id']. ">" . $row['Name'] . "</option>";
					}
		?> 
		</select>

		</br></br>
		Kilometerstand: </br>
		<input name="Kilometerstand" ></input> km (keine Trennzeichen, wie ".", bitte nur Ziffern Bsp.: 165000)
		</br>
		Erstzulassung: </br>
		
		<!--Bootstrap Date-Picker via https://formden.com/form-builder/ -->
		<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
		<div class="bootstrap-iso">
		 <div class="container-fluid">
		  <div class="row">
		   <div class="col-md-6 col-sm-6 col-xs-12">
			 <div class="form-group ">
			  <label class="control-label " for="date">
			  </label>
			  <div class="input-group">
			   <div class="input-group-addon">
				<i class="fa fa-calendar">
				</i>
			   </div>
			   <input class="form-control" id="date" name="Erstzulassung" placeholder="DD.MM.JJJJ" type="text"/>
			  </div>
			 </div>
			 
		   </div>
		  </div>
		 </div>
		</div>
		
		</br>
		TÜV/HU: </br>
		<!--Bootstrap Date-Picker via https://formden.com/form-builder/ -->
		<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
		<div class="bootstrap-iso">
		 <div class="container-fluid">
		  <div class="row">
		   <div class="col-md-6 col-sm-6 col-xs-12">
			 <div class="form-group ">
			  <label class="control-label " for="date">
			  </label>
			  <div class="input-group">
			   <div class="input-group-addon">
				<i class="fa fa-calendar">
				</i>
			   </div>
			   <input class="form-control" id="date" name="HU" placeholder="DD.MM.JJJJ" type="text"/>
			  </div>
			 </div>
			 
		   </div>
		  </div>
		 </div>
		</div>
		
		
		<!--
		<input name="HU"></input> Datum des nächsten TÜVs/HU Format JJJJ-MM-DD
		</br>  -->
		Preis: </br>
		<input name="Preis"></input> Euro
		</br>
		Bildpfad: </br>
		<input name="Bildpfad"></input> (.jpg wird automatisch angehangen)
		</br> </br>
		Anzahl Türen
		<input type="radio" name="AnzTueren" value="3">3
		<input type="radio" name="AnzTueren" value="5">5
		</br>
	
		<!--   <input type="submit" value="Speichern"/>  -->
		<div class="form-group">
			  <div>
			   <button class="btn btn-primary " name="submit" type="submit">
				Speichern
			   </button>
			  </div>
			 </div>
		
	</form>
	<a class="list1" href="logout.php"> Logout </a>
</body>
</html>