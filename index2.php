<!DOCTYPE html>
<html>
<head>
	 <!-- HTML5 --> <meta charset="utf-8"> <!-- HTML 4.x --> <meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
  
  <!-- Bootstrap from MAX-CDN see http://getbootstrap.com/getting-started/, viewed 2017-03-16 -->
  <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<!-- jQuery  -->
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	
	<!-- Bootstrap Date-Picker Plugin for future use 
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/> -->
	
	
	
	   
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
	  height: 40%;
	  width: auto;
      margin: auto;
  </style>
<html lang="de">
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<body>

<div class="header">
<h1>Gebrauchtwagen-Handel Ennepe Ruhr</h1>
preiswerte, zuverlässige Gebrauchtwagen, amerikanische und deutsche Sportwagen!
	<!--Rows must be placed within a .container (fixed-width) or .container-fluid (full-width) for proper alignment and padding.
Use rows to create horizontal groups of columns.
Content should be placed within columns, and only columns may be immediate children of rows. -->
	<div class="container">
	  <br>
	  <div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
		  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		  <li data-target="#myCarousel" data-slide-to="1"></li>
		  <li data-target="#myCarousel" data-slide-to="2"></li>
		  <li data-target="#myCarousel" data-slide-to="3"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
		  <div class="item active">
				<img src="img/audi-a3-h175.jpg" alt="Audi A3">
		  </div>

		  <div class="item">
				<img src="img/bmw-m4-h175.jpg" alt="BMW M4">
		  </div>
		
		  <div class="item">
				<img src="img/Ford-Mustang-2015-blau-h175.jpg" alt="Ford Mustang">
		  </div>

		  <div class="item">
				<img src="img/opel-corsa-Edition2000cool-h175.JPG" alt="Opel Corsa">
		  </div>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		  <span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		  <span class="sr-only">Next</span>
		</a>
	  </div>
	  		<button>Bilderkarussel AN/AUS</button>
		<script>
		$("#myCarousel").toggle(false);;
		$("button").click(function() {
			$("#myCarousel").toggle();
		});
		</script>
	</div>
</div>

<div class="row">
	<!-- 		3 spalten --- 9 spalten --- --- ---  				-->
	<div class="col-3 menu">
		<div class="list-group">
		    Autosuche (noch in Bearbeitung)
			<a class="list-group-item" href="index2.php?view=sortedByHUDateDescending">Autos sortiert nach neustem HU/TUEV-Datum</a>
			<a class="list-group-item" href="index2.php?view=sortedByPriceAscending">Autos sortiert nach Preis aufsteigend</a>
			<a class="list-group-item" href="index2.php?view=sortedByPowerKwAscending">Autos sortiert nach Leistung aufsteigend</a>
			<a class="list-group-item" href="login.php"> Login für Mitarbeiter </a>
		</div>		
	</div>


	<div class="col-9">
	<?php
	// unsere Klassen einbinden
	include('classes/controller.php');
	include('classes/model/model.php');
	include('classes/view.php');

	// $_GET und $_POST zusammenfasen, $_COOKIE interessiert uns nicht.
	$request = array_merge($_GET, $_POST);
	// Controller erstellen
	$controller = new Controller($request);
	// Inhalt der Webanwendung ausgeben.
	echo $controller->display();
	?>
	</div>

</div>
</body>
</html>

