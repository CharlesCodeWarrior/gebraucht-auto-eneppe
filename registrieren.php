<?php
session_start();
$pdo = new PDO('mysql:host=localhost; dbname=mydb', 'root', 'heinrich744');
?>
<!DOCTYPE html> 
<html> 
<head>
  <title>Registrierung</title>	
  <html lang="de">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head> 
<body>

<?php
$showFormular = true; //Variable ob das Registrierungsformular anezeigt werden soll

if (isset ($_GET['register'])) {
	$error = false;
	$email = $_POST['email'];
	$passwort = $_POST['passwort'];
	$passwort2 = $_POST['passwort2'];
	
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		echo 'Bitte eine gültige E-Mail-Adresse eingeben<br>';
		$error=true;
	}
	if (strlen($passwort) === 0) {
		echo 'Bitte ein Passwort angeben<br>';
		$error = true;
	}
	if($passwort != $passwort2) {
		echo 'Die Passwörter müssen übereinstimmen<br>';
		$error = true;
	}
	//Überprüfe, dass die E-Mail-Adresse noch nicht registriert wurde
	if (!$error) {
		$statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
		/* input_parameters

    An array of values with as many elements as there are bound parameters in the SQL statement being executed. All values are treated as PDO::PARAM_STR. http://php.net/manual/de/pdostatement.execute.php */
		$result = $statement->execute(array ('email' => $email));
		$user = $statement->fetch();
		
		if ($user !== false)  {
			echo 'Diese E-Mail-Adresse ist bereits vergeben<br>';
			$error = true;
		}
	}
	
		//Keine Fehler, wir können den Nutzer registrieren
	if(!$error) {	
		$passwort_hash = password_hash($passwort, PASSWORD_DEFAULT);
		$statement = $pdo->prepare("INSERT INTO users (email, passwort) VALUES (:email, :passwort)");
		$result = $statement->execute(array('email' => $email, 'passwort' => $passwort_hash));
		
		if($result) {		
			echo 'Du wurdest erfolgreich registriert. <a href="login.php">Zum Login</a>';
			$showFormular = false;
		} else {
			echo 'Beim Abspeichern ist leider ein Fehler aufgetreten<br>';
		}
	} 
	
	
}


if ($showFormular) {
?>

<form action="?register=1" method="post">
Email: <br>
<input type="email" size="40" maxlength="250" name="email"><br><br>

Dein Passwort:<br>
<input type="password" size="40" maxlength="250" name="passwort"><br>

Passwort wiederholen:<br>
<input type="password" size="40" maxlength="250" name="passwort2"><br><br>
 
<input type="submit" value="Abschicken">
</form>

<?php 
} //Ende von if($showFormular)
?>

</body>
<!-- 
Wir starten die Registrierung indem wir zuerst session_start() aufrufen und dann eine Verbindung zur Datenbank aufbauen. Hier haben wir als Datenbank test gewählt mit dem Datenbanknutzer root und einem leeren Passwort.

Das Formular zur Registrierung beginnt ab Zeile 50. Um zu steuern ob wir das Registrierungsformular ausgeben wollen haben wir die Variable $showFormular definiert. Standardmäßig (Zeile 13) ist der Wert true. Das Registrierungsformular ist ein schlichtes HTML-Formular welches die Daten an die eigene Seite sendet, aber zusätzlich noch den GET-Parameter register=1 mit übergibt. So können wir später leicht überprüfen, ob Daten für die Registrierung gesendet wurde (Zeile 15).

Mittels isset($_GET['register']) in Zeile 15 überprüfen wir ob der GET-Parameter übergeben wurde, sprich, ob das Formular für die Registrierung abgesendet wurde. Danach fragen wir die Daten aus dem Formular ab und überprüfen zum einem mittels filter_var() ob die E-Mail-Adresse gültigt ist und dass die zwei Passwörter identisch sind.  Sollte kein Fehler aufgetreten sein, so führen wir die Registrierung durch.

Ab Zeile 35 überprüfen wir, dass die E-Mail-Adresse noch nicht in der Tabelle vorhanden ist. Dazu senden wir einen entsprechenden SELECT-Query an die Datenbank und falls ein Benutzer gefunden wird, sprich, falls $user !== false, geben wir die Fehlermeldung zurück, dass die E-Mail-Adresse bereits vergeben ist.

Wie oben beschrieben berechnen wir erst mittels password_hash() den Hashwert des Passworts. Danach bereiten wir unseren SQL-Query vor zum Eintrag des neuen Nutzers (mehr Infos zum Thema Daten einfügen). Sollte alles geklappt haben, so geben wir eine Erfolgsmeldung aus und setzen die Variable $showFormular auf false. Dadurch verhindern wir im Erfolgsfall, dass das Registrierungsformular nochmal ausgegeben wird.
-->
</html>

