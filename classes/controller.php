<?php
class Controller {
	private $request = null;
	private $template = '';
	private $view = null;
	
	/**
	*Konstruktor, erstellt den Controller.
	*
	*@param Array $request Array aus $_GET & $_POST.
	*Dem Konstruktor werden die Daten, die der Seite mittels GET und POST �bergeben wurden, als Array $request �bergeben. Wenn dieses Array einen Eintrag $request['view'] enth�lt, wird dieser Eintrag als neues Template �bernommen. 
	*n�tzlich immer dann, wenn die index-seite mit anderen views aufgerufen wird: "default_tabelle","entry" z.b.
	*/
	public function __construct($request){
		$this->view = new View();
		$this->request = $request;
		$this->template = !empty($request['view']) ? $request['view'] : 'default_tabelle';	
	}
	
	/**
	*Das Template setzen.
	*@param name des Templates als String
	*/
	//pTemplate = parameterTemplate.
	public function setTemplate($pTemplate) {
		$this->template = $pTemplate;
	}
	
	/**
	*Methode zum Anzeigen des Contents.	
	*
	*@return String Content der Applikation
	*Die Funktion display() erstellt die View und entscheidet dann je nach Template(das im Konstruktor bestimmt wurde) welches Template geladen wird, welche Daten aus dem Model geladen und der View zugewiesen werden. Zu guter letzt wird der Inhalt der View mit der Methode loadTemplate() (siehe oben) zur�ckgegeben. 
	*/
	public function display(){
		//var f�r innere View
		$view = new View();		
		switch ($this->template){
			case 'entry':
				$view->setTemplate('entry');   //echo "$view" .$view;
				$entryid = $this->request['id']; //#t print_r(array_values($this->request))
				$entry = Model::getEntry($entryid); 		
				$view->assign('title', $entry['title']);
				$view->assign('content', $entry['content']);
				break;	
			case 'default_tabelle': 
				$entries = Model::getEntries();
				$view->setTemplate('default_tabelle');
				//key value. an entries sollen die Eintr�ge stehen
				$view->assign('entries',$entries);
				break;
			case  'sortedByHUDateDescending':
				$entries = Model::getEntriesByHU_Date();
				$view->setTemplate('default_tabelle');
				//key value. an entries sollen die Eintr�ge stehen
				$view->assign('entries',$entries);
				break;
			case  'sortedByPriceAscending':
				$entries = Model::getEntriesByPriceAscending();
				$view->setTemplate('default_tabelle');
				//key value. an entries sollen die Eintr�ge stehen
				$view->assign('entries',$entries);
				break;
			case  'sortedByPowerKwAscending':
				$entries = Model::getEntriesPowerKwAscending();
				$view->setTemplate('default_tabelle');
				//key value. an entries sollen die Eintr�ge stehen
				$view->assign('entries',$entries);
				break;
			case 'default':
			default:
				$entries = Model::getEntries();
				$view->setTemplate('default');
				//key value. an entries sollen die Eintr�ge sgtehen
				$view->assign('entries',$entries);
		}
		return $view->loadTemplate();
	}
}
?>