<?php
/**
 * Klasse für den Datenzugriff
 */
class Model{
	/*#e ein normales array, welches assoziatve arrays enthaelt (arrays, die verknüpfungen von schlüsseln mit werten beiinhalten))*/
	private static $eintraege = array(); 
	
	/**
	Initialisiert die Einträge, also holt sie aus der DB und sortiert entsprechend:
	@param $p_str_SQL String für die SQL-Anfrage Standarfall: "SELECT * FROM `autoangebote` aan
			JOIN automodelle am on (aan.am_f_id=am.am_id)"
	@param &andersrum Boolean, TRUE (eigentlich nur im Standarfall) für andersrum, also neueste einträge zuerst, FALSE für Sortierung so belassen
	@return 3d Array mit den Einträgen
	*/
	public static function init($p_str_SQL, $andersrum) {
		if (empty(self::$eintraege)) {
			/**
			*Anweisungen zur initialisierung
			erstellt DB Zugriff, liest Dagten aus db in $rs, schreibt sie
			in normales Array, das assoziatve Arrays enthält und kehrt evtl später die Reihenfolge des Arrays um, damit der aktuelleste Eintrag zu oberst steht
			*/
			include_once('connectionMy.php');
			$pdo = ConnectionMy::connect();
			//var_dump($pdo);
			
			//SQL-query
			$strSQL = $p_str_SQL;			
			
			//Query ausführen (Datensatzgruppe $rs enthaelt ergebnisse)
			$result = $pdo->query($strSQL);
			//var_dump($result);
			
			if (null != $result && false != $result && !empty($result)) {			
				/* #e (erklaerender kommi) Jede Zeile wird zu einem Array ($row), mit mysql_fetch_array ein 2 D Array aus Arrays mit jeweils array mit den daten eines eintrags . Update ist jetz ein 3d array
					bsp.  [(1) [( id=1) (title=mustang) [content [leistung km usw]]]
				*/
				$rows = $result->fetchAll();
				$eintraegeAusDB = array(count($result));
				$i = 0;
				foreach ($rows as $row) {		
					//Schreibe den Wert der Spalte Eintrag (der jetzt Array $row ist). Assoz. Array
					//idee für umstellung /verlagerung der formatierung in die view/templates
					//statt text assoziates array übergeben. auf jedenfall besser als unassozitiv
					/*"content"=>array("Name"=>$row['Name'], Kategorie=> ".$row['Kategorie']*/
					$eintraegeAusDB [$i] = array("id"=>$i, "title"=>$row['Name'], 
					"content"=> array(
					"Kategorie"=>$row['Kategorie'],"Hubraum"=>$row['Hubraum'], "Leistung"=>$row['Leistung'], "Kilometerstand"=>$row['Kilometerstand'], "Erstzulassung"=>$row['Erstzulassung'], "Preis"=>$row['Preis'],"Bildpfad"=> $row['Bildpfad'],"HU"=>$row['HU'],"AnzTueren"=> $row['AnzTueren']) );			
					$i++;
				}	
			} else {
				echo 'Fehler bei Datenbankabfrage';
			}			
			
			if ($andersrum) {
				/*#e in umgekehrter reihenfolge in array schreiben $eintraegeAndersrum, damit der neuste eintrag oberst angezeigt wird. neuster an stelle  n= $eintraegeAusDB-1, n -1, n-2... 
				n=0=aeltester eintrag */
				$eintraegeAndersrum = array();
				$j=0;
				for($i=count($eintraegeAusDB)-1; $i >= 0; $i--) {
					$eintraegeAndersrum[$j] = $eintraegeAusDB[$i];
					$j++;			
				}	
				//static klassenarray
				self::$eintraege = $eintraegeAndersrum;			
			} else {
				self::$eintraege = $eintraegeAusDB;	
			}			
			
			
			
		}
	}
	
	


	/**
	 * Gibt alle Einträge  zurück.
	 *
	 * @return Array3D einträgen.
	 */
	public static function getEntries(){		
		self::init("SELECT * FROM `autoangebote` aan
		JOIN automodelle am on (aan.am_f_id=am.am_id)", true);
		return self::$eintraege;
	}
	
	/**
	 * Gibt alle Einträge  zurück, sortiert absteigend nach HU/TÜV-Datum
	 * also Einträge (hier Autos), sortiert nach neuestem TÜV/HU
	 *
	 * @return Array3D einträgen.
	 */
	public static function getEntriesByHU_Date(){		
		self::init("SELECT * FROM autoangebote aan
		JOIN automodelle am on (aan.am_f_id=am.am_id) ORDER BY HU DESC", false);
		return self::$eintraege;
	}
	
	/**
	 * Gibt alle Einträge (momentan Autos)  zurück, sortiert aufsteigend nach Preis
	 * 
	 *
	 * @return Array3D einträgen.
	 */
	public static function getEntriesByPriceAscending(){		
		self::init("SELECT * FROM autoangebote aan
		JOIN automodelle am on (aan.am_f_id=am.am_id) ORDER BY PREIS ASC", false);
		return self::$eintraege;
	}
	
	/**
	 * Gibt alle Einträge (momentan Autos)  zurück, sortiert aufsteigend nach Leistung
	 * 
	 *
	 * @return Array3D einträgen.
	 */
	public static function getEntriesPowerKwAscending(){		
		self::init("SELECT * FROM autoangebote aan
		JOIN automodelle am on (aan.am_f_id=am.am_id) ORDER BY am.Leistung", false);
		return self::$eintraege;
	}
	
	

	/**
	 * Gibt einen bestimmten Eintrag zurück.
	 *
	 * @param int $id Id des gesuchten Eintrags
	 * @return Array Array, dass einen Eintrag repräsentiert, bzw. 
	 * 					wenn dieser nicht vorhanden ist, null.
	 */
	public static function getEntry($id){
		self::init("SELECT * FROM `autoangebote` aan
		JOIN automodelle am on (aan.am_f_id=am.am_id)", true);
		// da das array umgekehrt wurde muss der index auch umgekehrt werden sozusagen
		$id = $id - (count(self::$eintraege)-1);
		if ($id < 0) {
			$id = $id * -1;
		}
		if(array_key_exists($id, self::$eintraege)){			
			return self::$eintraege[$id];
		}else{
			return null;
		}
	}
}
?>