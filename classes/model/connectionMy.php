<?php
require 'conf.php'; 		
/* eine Art mini Wrapperklasse für msyql-connections.Ueber PDO. Sicherer! 
http://php.net/manual/de/intro.pdo.php*/
class ConnectionMy
{
    /** Wrapper Class for a PHP Database Oject on this system. Tries
	 * to connect do db, if fails returns false else the pdo object.
	 * Usage Example:
	* $result = $pdo->query($strSQL);
	*	var_dump($result);
	*
	*	if (false != $result && !empty($result)) {
	*	foreach ($rows as $row) {
	 *
	 * OR:
	 * sqlString = "INSERT INTO `zeiterfassung_items` (`id`, `user_id`, `date`, ) VALUES
	 * (NULL, :user , :date, )";
	* $statement = $pdo->prepare($sqlString);
    *	$result = $statement->execute(array('user'=> $user ,'date' => $date));
	*
	 * @return false|PDO
     */
	public static function connect() {
		
		$server = MYSQL_SERVER;
		$db = MYSQL_DATABASE;
		$pdo = false;
		try {
			$pdo = new PDO("mysql:host=$server;dbname=$db", MYSQL_USER, MYSQL_PASSWORD); 
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			echo 'Verbindung fehlgeschlagen: ' . $e->getMessage();
		}
		//var_dump($pdo);
		return $pdo;
	}
}
?>
